const questions = [
    "What would be your first question after waking up from being cryogenically frozen for 100 years?",
    "What was is the biggest lie you have told your parents?",
    "What was the last thing you googled?",
    "What was the last thing you bought on Amazon or bol.com?",
    "What could you give a 40-minute presentation on with absolutely no preparation?",
    "What would you rate 10 / 10?",
    "What is the most illegal thing you\'ve done?",
    "If you didn\'t have to sleep, what would you do with the extra time?",
    "What job would you be terrible at?",
    "If you had unlimited funds to build a house that you would live in for the rest of your life, what would the finished house be like?",
    "What province or country do you never want to go back to?",
    "If you could have your own Ben & Jerry flavor, what would it taste like and how would you name it?",
    "If the Netherlands was a dictatorship and you are the dictator, what would you do?",
    "What is the most annoying question that people ask you?",
    "Where is the most interesting place you\'ve been?",
    "How different was your life one year ago?",
    "Why did you decide to do the work you are doing now?",
    "What is the luckiest thing that has happened to you?",
    "What one thing do you really want but can\'t afford?",
    "What are you most likely to become famous for?",
    "What website do you visit most often?",
    "As the only human left on Earth, what would you do?",
    "Who impressed you the most and how?",
    "What did you think you would grow out of but haven\'t?",
    "What are some of the events in your life that made you who you are?",
    "What stereotype do you completely live up to?",
    "If you could make one rule that everyone had to follow, what rule would you make?",
    "What is something you can never seem to finish?",
    "Are you afraid of anything and why?",
    "What is something that your friends would consider \"so you\"?",
    "What personality trait do you value most and which do you dislike the most?",
    "What would be some of the most annoying things about having yourself as a roommate?",
    "What\'s the best and worst piece of advice you\'ve ever received?",
    "If the there was gonna be a zombie apocalypse what would you do?",
    "What are you really good at, but kind of embarrassed that you are good at it?",
    "What kind of comment or phrase would hurt your feelings?",
    "What do people think is weird about you?",
    "If you could make a 20 second phone call to yourself at any point in your life present or future, when would you call and what would you say?",
    "What do you have doubts about?",
    "What was something you used to believe as a child?",
    "What are the top three things you want to accomplish before you die? How close are you to accomplishing them?",
    "In your group of friends, what role do you play?",
    "What are you most insecure about?",
    "What was your last internet bookmark?",
    "If you woke up being the only person on the planet what would you do?",
    "If you were a superhero what powers would you have?",
    "If you didn\'t have to sleep, what would you do with the extra time?",
    "What is a trait you love most in people, and what is a trait you really dislike in people?",
    "If you won the lottery what would you do?"
  ]
  
  
  function getRandomItem (list) {
      const randomIndex = Math.floor(Math.random() * list.length)
      return list[randomIndex]
  }
  
  function getRandomPhrase () {
    return `${getRandomItem(questions)} `
  }
  
  function putPhraseOnThePage () {
    const h1 = document.querySelector('#phrase')
    h1.innerText = getRandomPhrase()
  }